var isValid = false

window.onload = function(){console.log('gotha dei')}

// function to set error (red color)
function setError(fieldsetID){
    
    fieldsetID.style = "border-color:red"
    console.log(`error at fieldset id: ${fieldsetID}`)
}

function setErrorlabel(labelID){
    console.log(labelID)
    labelID.style = "display: block"
}

//function to check whether the text input field is empty, null
function checkTextInputField(inputField,fieldset){
    if(inputField.value == "" || inputField.value == null){
        isValid = false
        inputField.focus()
        setError(fieldset)
    }
    else{
        isValid = true
    }
}

function checkNumberInputField(inputField,fieldset){
    var interizedValue = parseInt(inputField.value)
    if (interizedValue < 0 || interizedValue == 0 || inputField.value == "") {
        isValid = false
        inputField.focus()
        setError(fieldset)
    } else {
        isValid = true
    }
}

//function to check the select input fields
function checkSelectInputField(inputField,fieldset){
    if(inputField.value == "notselected"){
        inputField.focus()
        setError(fieldset)
        isValid = false
    }
}


//function to check whether the date 
//main function which declares the variables and makes calls
function validate(){    
    //f_volunteer_name
    console.log("so function is called")
    var volunteer_name = document.forms['join-form']['volunteer']
    checkTextInputField(volunteer_name,f_volunteer_name)
        
    var workStatus = document.forms['join-form']['work_status']
    if(workStatus.value == "contract"){
        var contractor_name = document.forms['join-form']['contractor_name']
        var contractor_address = document.forms['join-form']['cont_address']
        checkTextInputField(contractor_name,f_work_status_cont_name)
        checkTextInputField(contractor_address,f_work_status_cont_add)
    }
    
    else if(workStatus.value == "permanent"){
        var company_name = document.forms['join-form']['company_name']
        var company_address = document.forms['join-form']['company_address']
    checkTextInputField(company_name,f_perma_com_name)
    checkTextInputField(company_address,f_perma_com_name)
    }
    else{
        isValid = false
    }

    
    var member_name = document.forms['join-form']['name']
    checkTextInputField(member_name,f_person_name)
    
    
    var member_father_name = document.forms['join-form']['fname']
    checkTextInputField(member_father_name,f_person_fname)

    
    var dob = document.forms['join-form']['dob']
    // for now we use dob As text , will change later
    checkTextInputField(dob,f_dob)
    
    
    var id_no = document.forms['join-form']['idno']
    checkNumberInputField(id_no,f_id_no)
    
    
    var mem_gender = document.forms['join-form']['member_gender']
    checkSelectInputField(mem_gender,f_gender_m)

    
    var edu = document.forms['join-form']['education']
    checkTextInputField(edu,f_edu)
    
    
    var comm = document.forms['join-form']['community']
    checkTextInputField(comm,f_comm)
    
    
    var door = document.forms['join-form']['doorno']
    checkTextInputField(door,f_door)
    
    var colony = document.forms['join-form']['Locality']
    checkTextInputField(colony,f_colony)

    
    var taluk = document.forms['join-form']['taluk']
    checkTextInputField(taluk,f_taluk)
    
    
    var village = document.forms['join-form']['village']
    checkTextInputField(village,f_village)

    
    var post = document.forms['join-form']['postoffice']
    checkTextInputField(post,f_post)

    
    var dist = document.forms['join-form']['district']
    checkTextInputField(dist,f_dist)

    
    var city = document.forms['join-form']['city']
    checkTextInputField(city,f_city)
    
    var pin = document.forms['join-form']['pincode']
    checkTextInputField(pin,f_pin)

    var child_number = document.forms['join-form']['child_number']
    //var children = childrens[child_number]
    
    
    var cname1 = document.forms['join-form']['child_name1']
    checkTextInputField(cname1,f_cname1)

    
    var cgender1 = document.forms['join-form']['child_gender1']
    checkSelectInputField(cgender1,f_cgender1)

    
    var cage1 = document.forms['join-form']['child_age1']
    checkNumberInputField(cage1,f_cage1)

    
    var cq1 = document.forms['join-form']['child_qualification1']
    checkTextInputField(cq1,f_cq1)
    
    
    var cr1 = document.forms['join-form']['relation_type1']
    checkTextInputField(cr1,f_cr1)

    // getting other childs using loop xD
    if(child_number > 1){

        for(var iterator = 2; iterator < child_number; iterator++){
            var cname = document.forms['join-form']['child_name'+iterator]
            var cgender = document.forms['join-form']['child_gender'+iterator]
            var cage = document.forms['join-form']['child_age'+iterator]
            var cq = document.forms['join-form']['child_qualification'+iterator]
            var cr = document.forms['join-form']['relation_type'+iterator]
    
            var f_cname = 'name_fieldset'+iterator
            var f_cgender = 'gender_fieldset'+iterator
            var f_cage = 'age_fieldset'+iterator
            var f_cq = 'qual_fieldset'+iterator
            var f_cr = 'rela_fieldset'+iterator
    
            checkTextInputField(cname,f_cname)
            checkSelectInputField(cgender,f_cgender)
            checkNumberInputField(cage,f_cage)
            checkTextInputField(cq,f_cq)
            checkTextInputField(cr,f_cr)
        }
    }

    
    var c_work = document.forms['join-form']['current_work']
    checkTextInputField(c_work,f_c_work)

    
    var pay = document.forms['join-form']['payship']
    if(pay.value == "yes"){
        isValid = true
    }else if(pay.value == "no"){
        isValid = false
    }else{
        setError(f_pay)
        isValid = false
    }
    var pf = document.forms['join-form']['pf']
    if(pf.value == "yes"){
        isValid = true
    }else if(pf.value == "yes"){
        isValid = true
    }else{
        setError(f_pf)
        isValid = false
    }

    f_esi
    var esi = document.forms['join-form']['esi']
    if(esi.value == "yes"){
        isValid = true
    }else if(esi.value == "no"){
        isValid = true
    }else{
        setError(f_esi)
        isValid = false
    }

    
    var lwb = document.forms['join-form']['lwb']
    if(lwb.value == "yes"){
        var lwb_yes = document.forms['join-form']['lwb_yes']
        checkTextInputField(lwb_yes,lwb_yes_id)
    }else if(lwb.value == "no"){
        isValid = true
    }else{
        setError(f_lwb)
        isValid = false
    }
    

    var work_place = document.forms['join-form']['work_place']
    checkTextInputField(work_place,f_work_place)
    

    var salary_type = document.forms['join-form']['salary_type']
    if(salary_type.value == "daily"){
        isValid = true
    }else if(salary_type.value == "monthly"){
        isValid = true
    }else if(salary_type.value == "yearly"){
        isValid = true
    }else{
        setErrorlabel(label_fields_error)
        isValid = false
    }
    
    
    var salary = document.forms['join-form']['salary']
    checkNumberInputField(salary,f_salary)
    

    var working_hours = document.forms['join-form']['working_hours']
    checkNumberInputField(working_hours,f_working_hours)
    

    var experience = document.forms['join-form']['experience']
    checkNumberInputField(experience,f_experience)

    
    var doj_union = document.forms['join-form']['doj_union']
    checkTextInputField(doj_union,f_doj_union)
    
    var subscription = document.forms['join-form']['subscription']
    if(subscription.value == "yes"){
        isValid = true
    }else if(subscription.value == "no"){
        isValid = true
    }else{
        setError(f_subscription)
        isValid = false
    }
    
    var ration = document.forms['join-form']['ration_card']
    if(ration.value == "yes"){
        var rationNum = document.forms['join-form']['ration_card_no']
        checkTextInputField(rationNum,f_ration_card)
    }else if(ration.value == "no"){
        isValid = true
    }else{
        document.getElementById('ration_error').style = 'color:red;font-family:monspace;'
        isValid = false
    }

    var aadhar = document.forms['join-form']['aadhar']
    checkTextInputField(aadhar,f_aadhar)
    
    f_bank_account_name
    var bank_account_name = document.forms['join-form']['bank_account_name']
    checkNumberInputField(bank_account_name,f_bank_account_name)
    // document.getElementById('label_fields_bank_error').style = "display:block"
    // setError('label_fields_bank_error') Wut is this ?
    
    var bank_name = document.forms['join-form']['bank_name']
    checkTextInputField(bank_name,f_bank_name)
    
    var bank_branch = document.forms['join-form']['bank_branch']
    checkTextInputField(bank_branch,f_bank_branch)
    
    var ac_no = document.forms['join-form']['ac_no']
    checkNumberInputField(ac_no,f_ac_no)
    
    var ifsc_no = document.forms['join-form']['ifsc_no']
    checkNumberInputField(ifsc_no,f_ifsc_no)
    
    return isValid
}