$(document).ready(function () {
    $('#search_input').on("input", function () {
        query_text = $('#search_input').val();
        console.log(query_text);

        console.log($.trim(query_text).length );
        if ($.trim(query_text).length != 0)
        {

            $.ajax({
                data: {
                    query: query_text
                },
                type: 'POST',
                url: '/searchmembers',
                dataType: 'json'
            })
                .done(function (data) {
                    console.log(data)
                    var se = document.getElementById("search_suggestions");
                    var name = [];
                    for (i = 0; i < data.length; i++) {
                        name[i] = document.createElement("p");
                        name[i].innerHTML = data[i].name +' - '+ data[i].village;
                        name[i].setAttribute('id', 'se' + i);
                        se.appendChild(name[i]);
                        // document.append(se);
                        //console.log(data[i]);
                    }
    
                    // for (const name of data) {
                    //console.log(name);
                    // }
                })
        
        } 
        else
        {
            
                const myNode = document.getElementById("search_suggestions");
                while (myNode.firstChild) {
                  myNode.removeChild(myNode.firstChild);
                }
        }

       
    });
});