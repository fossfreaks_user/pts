// var x=1; 
// var c=1;

// shows label
function showlabel(elementid) {
  console.log("Element's Id is " + elementid)
  document.getElementById(elementid).style.display = "block";
}

function hidelabel(elementid) {
  document.getElementById(elementid).style.display = "none";
}

function rationcard_no() {
  document.getElementById("ration_card").style.display = "none";
  document.getElementById("f_ration_card").style.display = "none"
  
}

function rationcard_yes() {
  document.getElementById("ration_card").style.display = "block";
  document.getElementById("f_ration_card").style.display = "block"
}

function workstatus_cont() {
  document.getElementById("work_status_cont").style.display = "block";
  document.getElementById("work_status_permanent").style.display = "none";

}

function workstatus_permanent() {
  document.getElementById("work_status_permanent").style.display = "block";
  document.getElementById("work_status_cont").style.display = "none";

}

// function lwb_yes() {
//   document.getElementById("lwb_yes").style.display = "block";
// }

var x = 1;
function addchild() {
  x++
  var cn = document.getElementById('cno');
  cn.value = x;
  console.log(cn.value)
  console.log("now there are " + x + " Members in family ")

  //creation of fieldsets
  
  var hr = document.createElement('hr')
  hr.setAttribute('style','border-style:dashed;border-width:2px')
  
  var label = document.createElement('p')
  label.setAttribute('class','label')

  var fieldset_name = document.createElement('fieldset')
  fieldset_name.setAttribute('id','name_fieldset'+x)
  
  var fieldset_age = document.createElement('fieldset')
  fieldset_age.setAttribute('id','age_fieldset'+x)
  
  var fieldset_gender = document.createElement('fieldset')
  fieldset_gender.setAttribute('id','gender_fieldset'+x)
  
  var fieldset_qualification = document.createElement('fieldset')
  fieldset_qualification.setAttribute('id','qual_fieldset'+x)
  
  var fieldset_relationship = document.createElement('fieldset')
  fieldset_relationship.setAttribute('id','rela_fieldset'+x)

  //creation of label
  var name_label = document.createElement('legend')
  var gender_label = document.createElement('legend')
  var age_label = document.createElement('legend')
  var qualification_label = document.createElement('legend')
  var relationship_label = document.createElement('legend')

  //setting attributes and text to label
  label.innerHTML = `Family member ${x} Details`

  name_label.setAttribute('id','cname_label'+x)
  name_label.innerHTML = "Name of Family Member"

  age_label.setAttribute('id','cage_label'+x)
  age_label.innerHTML = "Age of Family Member"

  gender_label.setAttribute('id','cgender_label'+x)
  gender_label.setAttribute('class','nohide')
  gender_label.innerHTML = "Gender of Family Member"

  qualification_label.setAttribute('id','cq_label'+x)
  qualification_label.innerHTML = "Qualificaion of Family Member"

  relationship_label.setAttribute('id','cr_label'+x)
  relationship_label.innerHTML = "Relation with Family Member"

  //adding fieldsets with legends first
  
  fieldset_name.appendChild(name_label)
  fieldset_age.appendChild(age_label)
  fieldset_gender.appendChild(gender_label)
  fieldset_qualification.appendChild(qualification_label)
  fieldset_relationship.appendChild(relationship_label)
  
  //appending these at first :)
  
  var childs = document.getElementById("children")
  childs.appendChild(hr)
  childs.appendChild(label)
  childs.appendChild(fieldset_name)
  childs.appendChild(fieldset_age)
  childs.appendChild(fieldset_gender)
  childs.appendChild(fieldset_qualification)
  childs.appendChild(fieldset_relationship)

  //Name of member
  var cname = document.createElement('input')
  cname.setAttribute('type','text')
  cname.setAttribute('name','child_name'+x)
  cname.setAttribute('placeholder','Name of Family Member')
  cname.setAttribute('onfocus','showlabel(cname_label'+x+'.id)')
  cname.setAttribute('onblur','hidelabel(cname_label'+x+'.id)')
  
  //gender of child
  var cgender = document.createElement('select');
  cgender.setAttribute('name','child_gender'+x)
  cgender.options[0] = new Option('Select gender','notselected')
  cgender.options[1] = new Option('Male', 'male');
  cgender.options[2] = new Option('Female', 'female');
  cgender.options[3] = new Option('Transgender', 'tg');
  cgender.options[4] = new Option('Not to mention', 'ntm');
  cgender.setAttribute('class', 'child')

  //age of child
  var cage = document.createElement('input');
  cage.setAttribute('type', 'text');
  cage.setAttribute('name', 'child_age' + x);
  cage.setAttribute('placeholder', 'Age of Family member')
  cage.setAttribute('class', 'child')
  cage.setAttribute('pattern','[0-9]*')
  cage.setAttribute('onfocus','showlabel(cage_label'+x+'.id)')
  cage.setAttribute('onblur','hidelabel(cage_label'+x+'.id)')


  //child Qualification
  var cq = document.createElement('input');
  cq.setAttribute('type', 'text');
  cq.setAttribute('list','edu')
  cq.setAttribute('name', 'child_qualification' + x);
  cq.setAttribute('placeholder', 'Qualification of Family member')
  cq.setAttribute('class', 'child')
  cq.setAttribute('onfocus','showlabel(cq_label'+x+'.id)')
  cq.setAttribute('onblur','hidelabel(cq_label'+x+'.id)')


  //child relationship
  var cr = document.createElement('input')
  cr.setAttribute('type', 'text')
  cr.setAttribute('list', 'relation')
  cr.setAttribute('name', 'relation_type' + x)
  cr.setAttribute('placeholder', 'Type of relation')
  cr.setAttribute('class', 'child')
  cr.setAttribute('onfocus','showlabel(cr_label'+x+'.id)')
  cr.setAttribute('onblur','hidelabel(cr_label'+x+'.id)')

  
  // adding text fields to appropirate fields 
  document.getElementById('name_fieldset'+x).appendChild(cname)
  document.getElementById('gender_fieldset'+x).appendChild(cgender)
  document.getElementById('age_fieldset'+x).appendChild(cage)
  document.getElementById('qual_fieldset'+x).appendChild(cq)
  document.getElementById('rela_fieldset'+x).appendChild(cr)

  return false
}
//changing type from text into date 
function changetype(inputID) {
  document.getElementById(inputID).type = "date"
  console.log("date toggle")
}
//changing type from date to text
function changetypeT(inputID) {
  document.getElementById(inputID).type = "text"
  console.log("text toggle")
}

//scroll function
window.onscroll = function(){
  console.log("window scroll agthu")
  if(document.body.scrollTop > 50 || document.scrollingElement.scrollTop > 50){
    this.shrinkHeader()    
  }else{   
    this.expandHeader()
  }
}

function shrinkHeader(){
  document.getElementById("holder-header").style = "position:fixed;width:100%;margin-top: -10px;"
}

function expandHeader(){
  document.getElementById("holder-header").style = "position:relative;width:100%;margin-top: -10px;"
}
function goSearch(){
  window.location = "/search"
}
function goD(){
  window.location = "/downloadall"
}