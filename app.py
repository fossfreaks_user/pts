from flask import Flask, render_template, session, url_for, request, redirect, jsonify, send_from_directory
import pymongo
import json
import os
import sys
import re
import pandas
from pymongo import MongoClient
from pandas.io.excel import ExcelWriter
from bson.json_util import dumps
app = Flask(__name__)

client = MongoClient('localhost', 27017)
ptsdb = client["pts"]

@app.route('/')
def index():
    if 'username' in session:
        return render_template('join.html')
    return render_template('index.html')


@app.route('/login', methods=['POST'])
def login():
    # users = client.pts.users 
    # login_user = users.find_one({'name' : request.form['username']})
    login_user ='mks'
    password = request.form['pass']
    temppass = 'aaaa'
    if login_user is not None:
        # if users.find_one({'password':hashpass}):
        if temppass == password:
            session['username'] = request.form['username']
            return redirect(url_for('index')) 

    return 'Invalid username/password combination'


@app.route('/searchmembers',methods=['POST'])
def searchmembers():
    request_query=request.form['query']
    regex = re.compile('.*'+request_query+'.*', re.IGNORECASE) 
    query_name_and_village=client.pts.members.find({"name":regex},{"_id":0,"name":1,"village":1})

    result_query_name_village=dumps(query_name_and_village)
    print(result_query_name_village)
    return result_query_name_village





#search option edho work aagudu.. 
@app.route('/searchmembersX',methods=['POST'])
def searchmembersX():
    req_query=request.form['query']
    return_list=[]
    return_list1=[]
   # print(req_query)

    query_result=client.pts.members
    rgx = re.compile('.*'+req_query+'.*', re.IGNORECASE)  # compile the regex

  
   # print(re.escape(search_pattern))
    for x in query_result.find({"name":rgx,"village":rgx},{"name":1,"_id":0,"village":1}):
       # parse x:
       y = json.dumps(x,indent=2, sort_keys=True)
       k = json.dumps(x,indent=2, sort_keys=True)

       # the result (y) is a Python dictionary:
       data1=json.loads(k)
       data=json.loads(y)
       village=data1['village']
       name=data['name']
    #   print("oo:"+name) 
       return_list.append(name)
       return_list1.append(village)
       print('ola'+village)
    print(return_list) #idula paru..query result varudha nu
    return jsonify(return_list, return_list1)

#lets go to search page nibba
@app.route('/search')
def goSearch():
    print("function called")
    return render_template('search.html')

@app.route('/success', methods=['POST'] )
def join():
    members = client.pts.members
    members_child = client.pts.memberschilds
    volunteer = request.form['volunteer']
    name = request.form['name']
    fname = request.form['fname']
    idno = request.form['idno']
    dob = request.form['dob']
    m_gender = request.form['member_gender']
    education = request.form['education']
    community = request.form['community']
    door_number = request.form['doorno']
    locality = request.form['Locality']
    city = request.form['city']
    pincode = request.form['pincode']    
    village = request.form['village']
    post = request.form['postoffice']
    taluk = request.form['taluk']
    district = request.form['district']
    #Recent changes
    work_status = request.form['work_status']
    if work_status == "contract":
        contractor = request.form['contractor_name']
        company = "null"
        Address = request.form['cont_address']
    if work_status == "permanent":
        company = request.form['company_name']
        contractor = "null"
        Address = request.form['company_address']
    payship = request.form['payship']
    pf = request.form['pf']
    esi = request.form['esi']
    lwb = request.form['lwb']
    if lwb == "yes":
        lwb_doj = request.form['lwb_yes']
    else:
        lwb_doj = "Not join lwb yet"

    child_no = int(request.form['child_number'])
    # child_name = request.form['child_name1']
    # child_gender = request.form['child_gender1']
    # child_age = request.form['child_age1']
    # child_education = request.form['child_qualification1']
    childs=[]
    print(type(child_no))
    for i in range (child_no):
        # children={}
        # children['name']=
        # children['age']=
        # children['genger']=
        # children['qualification']=
        # children['relation_type']=
        # json_data=json.dumps(children)
        # childs.append(children)
        c_cname = request.form['child_name'+str(i+1)]
        c_cage = request.form['child_age'+str(i+1)]
        c_cgender = request.form['child_gender'+str(i+1)]
        c_cqual = request.form['child_qualification'+str(i+1)]
        c_creal = request.form['relation_type'+str(i+1)]
        members_child.insert({"Member_Reference_Id":idno,"Family_member_name":c_cname,"Family_Member_Relation":c_creal,"Family_Member_age":c_cage,"Family_Member_Qualification":c_cqual}) 


    ration = request.form['ration_card']
    house = request.form['house']
    current_work = request.form['current_work']
    work_place = request.form['work_place']
    # itha check panu da , na itha matum mathirukan da Olga paru da
    salary_type = request.form['salary_type']
    salary_paid = request.form['salary']
    working_hours = request.form['working_hours']
    experience = request.form['experience']
    doj = request.form['doj_union']
    subscription = request.form['subscription']
    aadhar = request.form['aadhar']
    ration_no = "null"
    bank_ac_name = request.form['bank_account_name']
    bank_name = request.form['bank_name']
    branch = request.form['bank_branch']
    ac_no = request.form['ac_no']
    ifsc_no = request.form['ifsc_no']
    if ration == "yes":
        ration_no = request.form['ration_card_no']
    if ration == "no":
        ration_no = "null"
    members.insert({"volunteer":volunteer,"name":name,"father_name":fname,"id_no":idno,"date_of_birth":dob,"member_gender":m_gender,"education":education,"community":community,"doorno":door_number,"locality":locality,"village":village,"post":post,"taluk":taluk,"district":district,"pincode":pincode,"city":city,"house":house,"current_work":current_work,"work_place":work_place,"salary_type":salary_type,"salary":salary_paid,"working_hours":working_hours,"experience":experience,"date_of_joining":doj,"subcription":subscription,"aadhar":aadhar,"ration_no":ration_no,"bank_account_name":bank_ac_name,"bank_name":bank_name, "branch":branch, "account_no":ac_no, "ifsc_no":ifsc_no, "no_of_children":child_no, "work_status":work_status, "contractor":contractor, "Address":Address, "company":company, "payship":payship, "pf":pf ,"esi":esi, "lwb":lwb, "lwb_doj":lwb_doj })
    # i  have appended childrens in seprate collection `members_child` @134 line
    return render_template('success.html',name=name,child_no=child_no)

#fetches db as csv from database
def getCsv(databse_name,collection_name):
    print(os.system('pwd'))
    os.system('mongoexport --db=pts --collection=members --type=csv --out=static/database/members.csv --fields=volunteer,work_status,contractor,company,Address,name,father_name,date_of_birth,id_no,member_gender,education,community,doorno,locality,taluk,village,post,city,district,house,current_work,payship,pf,esi,lwb,lwb_doj,work_place,salary_type,salary,subcription,ration_no,bank_account_name,bank_name,ifsc_no,account_no')
    os.system('mongoexport --db=pts --collection=memberschilds --type=csv --out=static/database/members_family.csv --fields=Member_Reference_Id,Family_member_name,Family_Member_Relation,Family_Member_age,Family_Member_Qualification')

def to_spreadsheet():
    csvFile = pandas.read_csv('static/database/members.csv')
    child_csvFile = pandas.read_csv('static/database/members_family.csv')
    csvFile = csvFile.rename(columns={'id_no':'Member Id','name':'Member`s Name','volunteer':'volunteer`s name','work_status':'work status','contractor':'contractor`s Name','company':'company`s Name','Address':'Address of work','father_name':'Father Name','date_of_birth':'Date of Birth','member_gender':'Member`s Gender','education':'Educational Qualification','community':'Member`s Community','doorno':'Door no','locality':'Locality','taluk':'Taluk','village':'Village','house':'House','current_work':'Current Work','payship':'Payship','pf':'is PF available','esi':'is ESI available','lwb':'Member of labour Welfare Board','lwb_doj':'Date of join in LWB','work_place':'Place of Work','salary_type':'Wage type','salary':'Salary','subcription':'Subcription Type','ration_no':'Ration card','bank_account_name':'Name in Bank account','bank_name':'Bank Name','ifsc_no':'IFSC number','account_no':'Account Number'})
    child_csvFile = child_csvFile.rename(columns={'Member_Reference_Id':'Member Reference Id','Family_member_name':'Family Member Name','Family_Member_Relation':'Family Member Relation','Family_Member_age':'Family Member age','Family_Member_Qualification':'Family Member Qualification'})
    csvFile.to_csv("static/database/members_list.csv",index=False)
    child_csvFile.to_csv("static/database/members_child_list.csv",index=False)
    print("scuccesfully converted to Formatted CSV files")
    csvFilesX = ["static/database/members_list.csv","static/database/members_child_list.csv"]
    with ExcelWriter("static/database/PTS_members.xlsx") as xlWriter:
        isa = 1
        for CSVfile in csvFilesX:
            if(isa == 2):
                xChar = "Family Details"
            if(isa == 1):
                xChar = "Member Details"
            pandas.read_csv(CSVfile).to_excel(xlWriter,sheet_name=xChar)
            isa = isa + 1


#downloads the whole database as spreadsheet
@app.route('/downloadall')
def downloadall():
    getCsv('pts','memebers')
    print("Exported Database Successfully")
    to_spreadsheet()
    print("converted to xlsx Files")
    return render_template('download.html',downloadlinkX="static/database/PTS_members.xlsx",downloadlinkCSV0="static/database/members_list.csv",downloadlinkCSV1="static/database/members_child_list.csv")

@app.route('/join', methods=['POST'] )
def backtoregister():
    return render_template('join.html')

if __name__ == "__main__":
    app.secret_key = 'pts!@#$();'
    app.run(host='0.0.0.0', port=4000, debug=True)
# for favicon
app.add_url_rule('/favicon.ico',redirect_to=url_for('static',filename='asserts.ico'))
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path,'static'),'favicon.ico',mimetype='image/vnd.microsoft.icon')

